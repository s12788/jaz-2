package domain;

public enum Role {
	
	NORMAL, PREMIUM, ADMINISTRATOR
}
