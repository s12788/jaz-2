package webapp;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import repository.DummyUserRepository;
import domain.User;

/**
 * Servlet implementation class AddApplicantServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		User user = retrieveUserFromRequest(request);
		
		session.setAttribute("logged", user);
		
		DummyUserRepository.setLoggedUser(user);
		
		response.sendRedirect("http://localhost:8080/profile");
		

	}
	
	private User retrieveUserFromRequest (HttpServletRequest request){
		
		User result = new User();
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("password"));
		return result;
	}

}
