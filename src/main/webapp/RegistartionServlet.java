package webapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repository.DummyUserRepository;
import domain.Role;
import domain.User;

/**
 * Servlet implementation class AddApplicantServlet
 */
@WebServlet("/registration")
public class RegistartionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		User user = retrieveUserFromRequest(request);
		user.setRole(Role.NORMAL);
		DummyUserRepository.add(user);
		
		String registartionComplete = ""
				+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" "
				+ "\"http://www.w3.org/TR/html4/loose.dtd\">"
				+ "<html>"
				+ "<head>"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
				+ "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"
				+ "<title>User profilr</title>"
				+ "</head>"
				+ "<body>"
				+"Registered succesfull!<br><br>"
				+"username: "+user.getUsername()
				+"<br>password: "+user.getPassword()
				+"<br>email: "+user.getEmail()
				+"<br><br>"
				+"<a href=\"index.jsp\">Back to main<a/>"
				+ "</body>"
				+ "</html>";
		
		PrintWriter out = response.getWriter();	
		out.println(registartionComplete);
		out.close();
		
		

	}
	
	private User retrieveUserFromRequest (HttpServletRequest request){
		
		User result = new User();
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("password"));
		result.setEmail(request.getParameter("email"));
		return result;
	}

}
