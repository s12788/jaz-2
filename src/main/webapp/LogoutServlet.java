package webapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		session.invalidate();
		
		String logout = ""
				+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" "
				+ "\"http://www.w3.org/TR/html4/loose.dtd\">"
				+ "<html>"
				+ "<head>"
				+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
				+ "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"
				+ "<title>User profilr</title>"
				+ "</head>"
				+ "<body>"
				+"You are successfully logged out!<br>"
				+"<br>"
				+"<a href=\"index.jsp\">Back to main<a/>"
				+ "</body>"
				+ "</html>";
		
		PrintWriter out=response.getWriter();
        out.println(logout); 
        out.close();  
	}

}
