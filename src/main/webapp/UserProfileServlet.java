package webapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import repository.DummyUserRepository;
import domain.Role;
import domain.User;


@WebServlet("/profile")
public class UserProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			
//			HttpSession session = request.getSession();
			response.setContentType("text/html");
			response.setCharacterEncoding("UTF-8");
			
			String setPremiumPage = "<a href=\"setPremium.jsp\">set premium account</a>";
			StringBuilder stringBuilder = new StringBuilder();
			
			User user = DummyUserRepository
					.checkUserByUsername(DummyUserRepository.loggedUser.getUsername());
					
			stringBuilder.append(""
					+ "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" "
					+ "\"http://www.w3.org/TR/html4/loose.dtd\">"
					+ "<html>"
					+ "<head>"
					+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>"
					+ "<link rel=\"stylesheet\" href=\"style.css\" type=\"text/css\" />"
					+ "<title>User profilr</title>"
					+ "</head>"
					+ "<body>"
					+"You are logged as: <br><br>"
					+"username: "+user.getUsername()
					+"<br>password: "+user.getPassword()
					+"<br>email: "+user.getEmail()
					+ (user.getRole().equals(Role.ADMINISTRATOR) ? "<br><br>"+setPremiumPage : "")
					+"<br><br> <a href=\"index.jsp\">Back to main<a/>"
					+ "</body>"
					+ "</html>");
			
			PrintWriter out = response.getWriter();	
			out.println(stringBuilder);
			out.close();
	}
}
