package webapp;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repository.DummyUserRepository;
import domain.User;


@WebServlet("/setPremium")
public class SetPremiumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		StringBuilder stringBuilder = new StringBuilder();
		
		String username = request.getParameter("username");
		if
		(DummyUserRepository.
				setPremium(username)){
			
			stringBuilder.append("<br>"+" "+username+
					" is now premium user!");
		}
		else{
			stringBuilder.append("<br>"+" User "+username+
					" not exits...");
		}
		
		stringBuilder.append("<br><br><a href=\"index.jsp\">Back to main<a/>");
		
		PrintWriter out = response.getWriter();	
		out.println(stringBuilder);
		out.close();
		
		
		}
	

		
		
		
	}

