package webapp.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import repository.DummyUserRepository;
import domain.Role;
import domain.User;


@WebFilter("/premium.jsp")
public class PremiumSessionFilter implements Filter {


	public void destroy() {
		
	}


	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httRequest = (HttpServletRequest) request;
		HttpSession session = httRequest.getSession();
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		
		if (session.getAttribute("logged")==null || DummyUserRepository.
				loggedUser.getRole().equals(Role.NORMAL)){
			httpResponse.sendRedirect("index.jsp");
			return;
		}

		chain.doFilter(request, response);
		
	}


	public void init(FilterConfig fConfig) throws ServletException {

	}

}
