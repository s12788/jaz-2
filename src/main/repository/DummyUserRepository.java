package repository;

import java.util.ArrayList;
import java.util.List;

import domain.User;
import domain.Role;

public class DummyUserRepository {
	
	
	static public User loggedUser;
	
	static public List<User> db 
	= new ArrayList <User>();
	
	User admin = new User();
	
	public DummyUserRepository(){
		admin.setUsername("admin");
		admin.setPassword("admin");
	}


	static public User checkUserByUsername(String username) {
		
		for (User c: db){
			
			if (c.getUsername().equals(username)){
				return c;
			}
		}
		
		return null;
	}
	
	static public void setLoggedUser(User user) {
		
		for (User c: db){
			
			if (c.getUsername().equals(user.getUsername())){
				loggedUser = c;
			}
		}
	}
	
	static public User verifyLoginUser(String username, String password) {
		
		for (User c: db){
			
			if (c.getUsername().equals(username) &&
					c.getPassword().equals(password)){
				return c;
			}
		}
		return null;
		
	}
	
	static public boolean setPremium(String username) {
		
		for (User c: db){
			
			if (c.getUsername().equals(username)){
				c.setRole(Role.PREMIUM);
				return true;
			}
		}
		
		return false;
	}


	static public void add(User user) {
		db.add(user);
	}


	static public void setPremiumUser(User user) {
		
		user.setRole(Role.PREMIUM);
	}
	
	static public void makeAdmin (){
		
		 boolean adminExist = false;
		   for (User c: DummyUserRepository.db){
			   if (c.getUsername().equals("admin")){
			   adminExist = true;
		   }
		   }
			if (!adminExist) {
				   domain.User admin = new User(); 
				   admin.setUsername("admin");
				   admin.setPassword("admin");
				   admin.setRole(Role.ADMINISTRATOR);
				   admin.setEmail("admin@a.net");
				   DummyUserRepository.add(admin);
			}
	}

}
