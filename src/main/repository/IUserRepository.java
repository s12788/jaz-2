package repository;

import domain.User;

public interface IUserRepository {
	
	public User checkUserByEmailAddress(String email);
	public void add (User user);
	public void setPremiumUser (User user);

}
