

import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import domain.User;
import repository.DummyUserRepository;
import webapp.LoginServlet;
import webapp.RegistartionServlet;

@RunWith(MockitoJUnitRunner.class)
public class TestLoginServlet extends Mockito {
	
	@InjectMocks 
	LoginServlet servlet;
	
	@Test
	public void servlet_should_write_info_that_user_logged()
		throws IOException, ServletException{
		
		System.out.println("a");
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		HttpSession session = mock(HttpSession.class);
		when(request.getSession()).thenReturn(session);
		servlet.doPost(request, response);
		verify(session).setAttribute(eq("logged"), Mockito.any(User.class));

	}

}
